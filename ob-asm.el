;;; ob-asm.el --- Org-babel functions for ASM languages -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Pierre-Antoine Rouby

;; Author: Pierre-Antoine Rouby <contact@parouby.fr>
;; Version: 0.0.1
;; Keywords: lisp, extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Org-babel functions for ASM languages.
;; - Currently only support x86_64 and x86 (not tested) processor.
;;   (If machine architecture is not implemented, use x86 by default.)
;; - Currently only support GNU assembler.
;; - Floating point register display, is not totally functional.

;;; Code:

(require 'asm-mode)
(require 'ob)

(defconst org-babel-header-args:asm '((registers . :any)
                                      (flags     . :any)
                                      (cmdline   . :any)
                                      (libs      . :any))
  "asm-specific header arguments.")

(defvar ob-asm-version "0.0.1")

(defcustom org-babel-asm-compiler "gcc"
  "Command used to compile a assembler source code file."
  :group 'org-babel
  :type 'string)

(defvar org-babel-asm-machine
  (cond
   ((string-match "x86_64" system-configuration) 'x86_64)
   ((string-match "x86"    system-configuration) 'x86)
   (t (message "ob-asm: %s arch is not implemented."
               system-configuration)
      nil))
  "Current machine architecture.")

(defvar org-babel-asm-push
  '((x86_64   . "pushq")
    (x86      . "pushl")
    (nil      . "push"))
  "Push instruction by architecture.")

(defvar org-babel-asm-pop
  '((x86_64   . "popq")
    (x86      . "popl")
    (nil      . "pop"))
  "Pop instruction by architecture.")

(defvar org-babel-asm-syscall
  '((x86_64   . "syscall")
    (nil      . "int $0x80"))
  "Syscall instruction by architecture.")

(defvar org-babel-asm-instructions
  `((push    . ,org-babel-asm-push)
    (pop     . ,org-babel-asm-pop)
    (syscall . ,org-babel-asm-syscall))
  "Assembler instructions.")

(defvar org-babel-asm-piece-of-code-ret-0
  '((x86_64  . "    xor    %rax, %rax\n    ret")
    (nil     . "    xor    %eax, %eax\n    ret"))
  "Piece of code to function return 0.")

(defvar org-babel-asm-integer-print-format
  "\"%s %%d #o%%o #x%%x\\n\""
  "Integer \"printf\" format. This string should be formated firstly
  with register name.")

(defvar  org-babel-asm-piece-of-code-printf-integer-format
  '((x86_64  . "
    mov    %%%s, %%rax
    lea    %s_ob_format(%%rip), %%rdi
    mov    %%rax, %%rsi
    mov    %%rax, %%rdx
    mov    %%rax, %%rcx
    xor    %%rax, %%rax
    call   printf\n")
    (nil     . "
    mov    %%%s, %%eax
    lea    %s_ob_format(%%eip), %%edi
    mov    %%eax, %%esi
    mov    %%eax, %%edx
    mov    %%eax, %%ecx
    xor    %%eax, %%eax
    call   printf\n"))
  "Piece of code to call \"printf\" function. There string must
containe 2 format args.")

(defvar org-babel-asm-float-print-format
  "\"%s %%f %%e %%g\\n\""
  "Floating point \"printf\" format. This string should be formated
  firstly with register name.")

(defvar  org-babel-asm-piece-of-code-printf-float-format
  '((x86_64  . "
    push      %%rbp
    movq      %%rsp, %%rbp
    subq      $16, %%rsp

    movss     %%%s, -4(%%rsp)
    cvtss2sd  -4(%%rsp), %%xmm2
    cvtss2sd  -4(%%rsp), %%xmm1
    cvtss2sd  -4(%%rsp), %%xmm0
    lea       %s_ob_format(%%rip), %%rdi
    mov       $3, %%rax
    call      printf

    addq      $16, %%rsp
    pop       %%rbp\n")
    (nil     . ""))
  "Piece of code to call \"printf\" function on float. There string
must containe 2 format args.")

(defvar  org-babel-asm-piece-of-code-save-xmm-reg
  '((x86_64  . "
    push      %rbp
    movq      %rsp, %rbp
    subq      $40, %rsp

    movss     %xmm0, -4(%rbp)
    movss     %xmm1, -8(%rbp)
    movss     %xmm2, -12(%rbp)\n")
    (nil     . ""))
  "Piece of code to call save xmm registers.")

(defvar  org-babel-asm-piece-of-code-restore-xmm-reg
  '((x86_64  . "
    movss     -12(%rbp), %xmm2
    movss     -8(%rbp),  %xmm1
    movss     -4(%rbp),  %xmm0

    addq      $40, %rsp
    pop       %rbp
    \n")
    (nil     . ""))
  "Piece of code to call restore xmm registers.")

(defvar org-babel-asm-caller-save-registers
  '((x86_64 . ("rax" "rcx" "rdx" "rsp" "rsi" "rdi"
               "r8" "r9" "r10" "r11"))
    (nil    . ("eax" "ecx" "edx")))
  "Caller save registers.")

(defvar org-babel-asm-callee-save-registers
  '((x86_64 . ("rbx" "rbp" "r12" "r13" "r14" "r15"))
    (nil    . ("ebx" "ebp" "esi" "edi")))
  "Callee save registers.")

(defvar org-babel-asm-float-registers
  '((x86_64 . ("xmm0" "xmm1" "xmm2" "xmm3" "xmm4"))
    (nil    . ("xmm0" "xmm1" "xmm2" "xmm3" "xmm4")))
  "Float registers.")

(defun org-babel-asm-get-registers (caller)
  "Get registers list for machine architecture. If CALLER is not
`nil', get caller save registers, else get callee save
registers."
  (let ((lst (assq org-babel-asm-machine
                   (if (not caller)
                       org-babel-asm-callee-save-registers
                     org-babel-asm-caller-save-registers))))
    (cdr lst)))

(defun org-babel-asm-get-instructions (ins)
  "Get instruction INS for machine architecture."
  (let ((ins-lst (assq ins org-babel-asm-instructions)))
    (if (not ins-lst)
        (progn
          (message "org-babel-asm: Unknown instructions %s\n"
                   ins)
          "")
      (let ((lst (assq org-babel-asm-machine (cdr ins-lst))))
        (if (not lst)
            (cdr (assq nil (cdr ins-lst)))
          (cdr lst))))))

(defun org-babel-execute:asm (body params)
  (org-babel-asm-execute-asm body params))

(defun org-babel-expand-body:asm (body params)
  (org-babel-asm-expand-asm body params))

(defun org-babel-asm-execute-asm (body params)
  "Execute BODY assembler piece of code."
  (let* ((tmp-src-file (org-babel-temp-file
			"asm-src-" ".s"))
	 (tmp-bin-file
	  (org-babel-process-file-name
	   (org-babel-temp-file "asm-bin-" org-babel-exeext)))
	 (cmdline (cdr (assq :cmdline params)))
	 (cmdline (if cmdline (concat " " cmdline) ""))
	 (flags (cdr (assq :flags params)))
	 (flags (mapconcat 'identity
			   (if (listp flags)
                               flags
                             (list flags))
                           " "))
	 (libs (org-babel-read
		(or (cdr (assq :libs params))
		    (org-entry-get nil "libs" t))
		nil))
	 (libs (mapconcat #'identity
			  (if (listp libs) libs (list libs))
			  " "))
	 (full-body
          (org-babel-asm-expand-asm body params)))
    (with-temp-file tmp-src-file (insert full-body))
    ;; Compile
    (org-babel-eval
     (format "%s -o %s %s %s %s"
             org-babel-asm-compiler
             tmp-bin-file
             flags
             (org-babel-process-file-name tmp-src-file)
             libs)
     "")
    ;; Exec
    (let ((results
	   (org-babel-eval
            (concat tmp-bin-file cmdline)
	    "")))
      (when results
	(setq results (org-trim (org-remove-indentation results)))
	(org-babel-reassemble-table
	 (org-babel-result-cond (cdr (assq :result-params params))
	   (org-babel-read results t)
	   (let ((tmp-file (org-babel-temp-file "asm-")))
	     (with-temp-file tmp-file (insert results))
	     (org-babel-import-elisp-from-file tmp-file)))
	 (org-babel-pick-name
	  (cdr (assq :colname-names params)) (cdr (assq :colnames params)))
	 (org-babel-pick-name
	  (cdr (assq :rowname-names params)) (cdr (assq :rownames params))))))))

(defun org-babel-asm-push-regs (registers)
  "Returns piece of code to save REGISTERS."
  (mapconcat
   (lambda (r)
     (format "    %s %%%s"
             (org-babel-asm-get-instructions 'push)
             r))
   registers
   "\n"))

(defun org-babel-asm-pop-regs (registers)
  "Returns piece of code to restore REGISTERS."
  (mapconcat
   (lambda (r)
     (format "    %s  %%%s"
             (org-babel-asm-get-instructions 'pop)
             r))
   (reverse registers)
   "\n"))

(defun org-babel-asm-push-xmm ()
  "Returns piece of code to save xmm registers."
  (cdr (assq org-babel-asm-machine
             org-babel-asm-piece-of-code-save-xmm-reg)))

(defun org-babel-asm-pop-xmm ()
  "Returns piece of code to restore xmm registers."
  (cdr (assq org-babel-asm-machine
             org-babel-asm-piece-of-code-restore-xmm-reg)))

(defun org-babel-asm-regs-save-wrap (body regs)
  "Wrap BODY with safe registers piece of code. REGS will be save
and restore."
  (format "%s%s\n%s\n%s%s\n"
          (org-babel-asm-push-regs regs)
          (org-babel-asm-push-xmm)
          body
          (org-babel-asm-pop-xmm)
          (org-babel-asm-pop-regs regs)))

(defun org-babel-asm-format-data-output (register)
  "Returns REGISTER string to display REGISTER value at runtime."
  (format "%s_ob_format:    .asciz %s"
          register
          (format
           (if (not (member register
                            (cdr (assq org-babel-asm-machine
                                       org-babel-asm-float-registers))))
               org-babel-asm-integer-print-format
             org-babel-asm-float-print-format)
           register)))

(defun org-babel-asm-format-print-output (register regs)
  "Returns safe wrapped assembler code to print the REGISTER
status. REGS is list of all registers to be printed (this
registers could be saved)."
  (let ((caller-regs (org-babel-asm-get-registers t)))
    (org-babel-asm-regs-save-wrap
     (format
      (cdr
       (assq org-babel-asm-machine
             (if (not (member register
                              (cdr (assq org-babel-asm-machine
                                         org-babel-asm-float-registers))))
                 org-babel-asm-piece-of-code-printf-integer-format
               org-babel-asm-piece-of-code-printf-float-format)))
      register register)
     ;; Reduction number of registers to save
     (remove 'nil
             (mapcar (lambda (reg)
                       (let ((m (member
                                 reg
                                 caller-regs)))
                         (if m reg nil)))
                     regs)))))

(defun org-babel-asm-safe-asm-fn-wrap (name body return)
  "Returns function NAME, with safe wrapped BODY and RETURN piece
of code."
  (format "%s:\n%s\n%s\n"
          name
          (org-babel-asm-regs-save-wrap
           body
           (org-babel-asm-get-registers nil))
          return))

(defun org-babel-asm-main-wrap (body regs)
  "Wrap BODY on main function, and display REGS value if REGS not
`nil'."
  (org-babel-asm-safe-asm-fn-wrap
   "main"
   ;; Main function body
   (concat "\n\n
##################################################################
### START OF PAYLOAD
##################################################################\n"
           body
           "
##################################################################
### END  OF  PAYLOAD
##################################################################\n\n"
           (if regs
               (mapconcat (lambda (x)
                            (org-babel-asm-format-print-output x regs))
                          regs "\n")
             ""))
   ;; Return 0
   (cdr (assq org-babel-asm-machine
              org-babel-asm-piece-of-code-ret-0))))

(defun org-babel-asm-ensure-main-wrap (body regs)
  "Wrap BODY in a \"main\" function call if none exists."
  (if (string-match ".text" body)
      body
    (format
     "
##################################################################
# Auto generate file
#   ob-asm: %s
#   emacs:  %s
#   date:   %s
##################################################################

.data
%s

.text
.global main
%s

##################################################################
# End here
##################################################################
"
     ob-asm-version
     emacs-version
     (format-time-string "%Y-%m-%d  %H:%M:%S")

     ;; Printf strings
     (mapconcat 'org-babel-asm-format-data-output
                regs "\n")
     ;; Main function
     (org-babel-asm-main-wrap body regs))))

(defun org-babel-asm-remove-duplications (sec)
  "Remove duplications on SEC."
  (pcase sec
    (`(,x . ,r)
     (if (not (member x r))
         (cons x (org-babel-asm-remove-duplications r))
       (org-babel-asm-remove-duplications r)))
    (_ '())))

(defun org-babel-asm-expand-asm (body params)
  "Wrap BODY on \"main\" function if is needed."
  (let*  ((registers (cdr (assq :registers params)))
          (regs (if registers
                    (split-string registers " ")
                  '())))
    (org-babel-asm-ensure-main-wrap
     body
     (org-babel-asm-remove-duplications regs))))

(provide 'ob-asm)

;;; ob-asm.el ends here
